<?php

namespace Codeifyr\Controllers;

use Codeifyr\Controllers\BaseController;
use Codeifyr\Services\View;


	class HomeController extends BaseController {

		public function __construct() {


			parent::__construct();

		}

		public function index() {

			View::setView('/views/home.php');

			return true;

		}



	}