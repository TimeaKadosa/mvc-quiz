<?php

namespace Codeifyr\Controllers;

use Codeifyr\Controllers\BaseController;
use Codeifyr\Models\UserModel;
use Codeifyr\Services\View;
use Codeifyr\Services\Password;
use Codeifyr\Services\Filehandler;

	class UserController extends BaseController {

		public $message;

		public function __construct() {

			$this->Model = new UserModel();
			$this->baseRoute = '/user';
			parent::__construct();
		}

		public function upload() {

			$id = $_SESSION['user']['id'];

			$user = $this->Model->fetch($id);
			$oldPicture =  $user['image'];

			$file = $_FILES['fileToUpload'];
			

			$fileHandler = new Filehandler();
			$upload = $fileHandler->imageUpload($file);

			# if something went wrong during the upload 
			if (!$upload) {

				$message['error'] = "Something went wrong";
				$this->show($id, $message);
				
			
			}
			# if upload was successful
			elseif ($upload) {

				//$fileName = $_FILES['fileToUpload']['name'];
				$fileName = array("image " => $_FILES['fileToUpload']['name']);
				// store filename in the database
				if($this->Model->update($fileName, $id)){

					if (!$oldPicture) {

						$message['success'] = "Profile picture changed successfully";
						$this->show($id, $message);	

						return true;
					}

					$target_path = PUBLIC_FOLDER. "/profileImages/". $oldPicture;
					
					chmod($target_path, 0777);
				 	unlink($target_path);

					$message['success'] = "Profile picture changed successfully";
					$this->show($id, $message);	

					return true;
				}
				
			}

		}


		public function index(){

			//$id = $_SESSION['user']['id'];

			$keyname = $this->baseName;

			$$keyname = $this->Model->fetch(25);

			return	View::setView($this->views . 'index.tpl',  compact($keyname));
		
			
		}

		public function show($id, $message = null) {


			if (!isset($_SESSION['user'])){

				header('location: /login');
			}


			$keyname = $this->baseName;

			$$keyname = $this->Model->fetch($id);

			return	View::setView($this->views . 'show.tpl', compact($keyname), $message);

		}


		public function loginform($message = null) {

		
			return View::setView($this->views. 'login.tpl', $data = null, $message);	

		}

		public function login() {

			# set the username and password (no validateing yet)
			$username = $_POST['username'];
			$password = Password::password_encrypt($_POST['password']);

			# Get the user by it's username and password
			$user = $this->Model->getUserByUsernamePassword($username, $password );

			#if valid user set user session
			if($user) {

				$_SESSION['user']['id'] 		= $user['id'];
				$_SESSION['user']['username'] 	= $user['username'];
				$_SESSION['user']['password'] 	= $password['password'];
				$_SESSION['user']['image'] 	= $user['image'];
				

				# redirect to users/1
				header('location: ' . $this->baseRoute );

			}
			else {


				$message['error'] = "Incorrect Username / Password";
				$this->loginform($message);


			}
		}


		public function logout() {

			session_destroy();
			header("location: /login" );
		}	

		public function store() {

			$input = $_POST;
			$type = array_pop($input);


			if($this->Model->insert($input)) {
					
					// insert related data to permission
					$id = $this->Model->getLastInsertId();

					$sql = "INSERT INTO permission (user_id, user_type) VALUES (:user_id, :user_type)";
					$binding = array('user_id' => $id ,'user_type' => $type);

					if($this->Model->execute($sql,$binding)) {
						
						$_SESSION['success']['message'] = "Thank you for your registration! Your account is now ready to use. ";
						header("location: /login" );
					}
					else{

						$_SESSION['error']['message'] = "Something went wrong !";
						header("location: /login" );
					}

					return true;
			}
		}
	}