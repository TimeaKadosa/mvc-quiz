<?php

namespace Codeifyr\Controllers;

use Codeifyr\Services\View;
use Codeifyr\Services\Password;


	class BaseController {

		use \Codeifyr\Traits\ReflectionInfoTrait;

		protected $Model;
		protected $views;
		//protected $baseRoute;
		protected $baseName;


		public function __construct() {

			$this->baseName = $this->reflectBaseName();
			/*$this->views = '/views/' . $this->baseName . '/';*/
			$this->views = $this->baseName . '/';
			$this->views;


		}


		public function edit($id) {

			$keyname = $this->baseName;

			$$keyname = $this->Model->fetch($id);

			return	View::setView($this->views . 'edit.tpl', compact($keyname));
		}


		public function update($id) {

			$input = $_POST;

			if($this->Model->update($input,$id)){

				$keyname = $this->baseName;

				$$keyname = $this->Model->fetch($id);

				$message['success'] = "Update was successful";
				// load view
				View::setView($this->views . 'edit.tpl', compact($keyname),$message);

				//unset($_SESSION['success']['message']);
				return true;
			}
			else {

				$_SESSION['error']['message'] = "Something went wrong !";
				// load view
				return View::setView($this->views . 'edit.tpl', compact($keyname));
			}
			
			

		}

	}