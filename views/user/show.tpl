<!-- Debug -->
{*$data.user|@var_dump*}


{include file='header.tpl'}

    <div class="container body">


        <div class="main_container">

            {include file='side_menu.tpl'}

             {include file='top_navigation.tpl'}

			   <div class="">
                    <div class="page-title">
                        <div class="title_left">
                            <h3>User Profile</h3>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>User Profile</h2>
                                    
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">

                                    <div class="col-md-3 col-sm-3 col-xs-12 profile_left">

                                    <!-- profile image -->
                                    <div class="profile_img">
    
                                        <div id="crop-avatar">
                                            <div class="image-upload" >
                                                
                                                <div id="popup" class="avatar-view" title="Change the avatar">
                                                    <img src="/profileImages/{if $data.user.image == ''}user-default.jpg{else}{$data.user.image}{/if}" alt="Avatar">
                                                </div>
 
                                            </div>
                                        </div>

                                        {include file='user/partials/profile_img_change.tpl'}

                                        {if isset($message['success']) }

                                                <div class="alert alert-success">
                                                    <div class="info">{$message['success']}</div>
                                                </div>
                                        {elseif isset($message['error']) }
                                                <div class="alert alert-danger">
                                                    <div class="info">{$message['error']}</div>
                                                </div>
                                        {/if}

                                    </div>
                                    <!-- /profile image -->
       
                                        <h3>{$data.user.name}</h3>

                                        <ul class="list-unstyled user_data">
                                            <li><i class="fa fa-map-marker user-profile-icon"></i> San Francisco, California, USA
                                            </li>

                                            <li>
                                                <i class="fa fa-at"></i> {$data.user.email}
                                            </li>

                                            <li class="m-top-xs">
                                                <i class="fa fa-phone"></i>
                                                {$data.user.phone}
                                            </li>
                                        </ul>

                                        <a href="/user/{$data.user.id}/edit" class="btn btn-success"><i class="fa fa-edit m-right-xs"></i>Edit Profile</a>
                                        <br />

          
                                </div>

                                <div class="col-md-9 col-sm-9 col-xs-12">

                                    <div class="profile_title">
                                        <div class="col-md-6">
                                            <h2>User Activity Report</h2>
                                        </div>
                                            
                                        </div>
                                        

                                        <div class="" role="tabpanel" data-example-id="togglable-tabs">
                                            <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                                <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Study Activity</a>
                                                </li>
                                                <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Forum Activity</a>
                                                </li>
                                                <li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Profile</a>
                                                </li>
                                            </ul>
                                            <div id="myTabContent" class="tab-content">

                                                <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="profile-tab">

                                                    <!-- start study activity -->
                                                    <table id="study_activity" class="data table table-striped no-margin">
                                                        <thead>
                                                            <tr>
                                                               
                                                                <th>Lessons</th>
                                                                <th>Chapters</th>
                                                                <th>Achievement</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                               
                                                                <td><a href="">Lesson 1 </a></td>
                                                                <td><a href="">Introducing PHP</a></td>
                                                                <td class="vertical-align-mid">
                                                                    <a href="" title="23% Completed">
                                                                        <div class="progress">
                                                                            <div class="progress-bar progress-bar-success" data-transitiongoal="35"></div>
                                                                        </div>
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                               
                                                                <td><a href="">Lesson 3 </a></td>
                                                                <td><a href="">PHP Language basics</a></td>
                                                                <td class="vertical-align-mid">
                                                                    <a href="" title="45% Completed">
                                                                        <div class="progress">
                                                                            <div class="progress-bar progress-bar-danger" data-transitiongoal="15"></div>
                                                                        </div>
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                
                                                                <td><a href="">Lesson 4 </a></td>
                                                                <td><a href="">Decisions and Loops</a></td>
                                                                <td class="vertical-align-mid">
                                                                    <a href="" title="37% Completed">
                                                                        <div class="progress">
                                                                            <div class="progress-bar progress-bar-success" data-transitiongoal="45"></div>
                                                                        </div>
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                               
                                                                <td><a href="">Lesson 5</a></td>
                                                                <td><a href="">Strings</a></td>
                                                                <td class="vertical-align-mid">
                                                                    <a href="" title="23% Completed">
                                                                        <div class="progress">
                                                                            <div class="progress-bar progress-bar-success" data-transitiongoal="75"></div>
                                                                        </div>
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <!--  start study activity -->

                                                </div>
                                                <div role="tabpanel" class="tab-pane fade " id="tab_content2" aria-labelledby="home-tab">

                                                    <!-- start forum activity -->
                                                    <ul class="messages">
                                                        <li>
                                                            
                                                            <div class="message_date">
                                                                <h3 class="date text-info">24</h3>
                                                                <p class="month">May</p>
                                                            </div>
                                                            <div class="message_wrapper">
                                                                <p>Topic title</p>
                                                                <h5 class="heading">11-06-2015 - 16:25:33 </h5>
                                                                <blockquote class="message">Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua butcher retro keffiyeh dreamcatcher synth.</blockquote>
                                                                <br />
                                                                <p class="url">
                                                                    <span class="fs1 text-info" aria-hidden="true" data-icon=""></span>
                                                                    <a href="#"></i> Read more... </a>
                                                                </p>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            
                                                            <div class="message_date">
                                                                <h3 class="date text-error">21</h3>
                                                                <p class="month">May</p>
                                                            </div>
                                                            <div class="message_wrapper">
                                                                <p>Topic title</p>
                                                                <h5 class="heading">13-06-2015 - 16:25:33 </h5>
                                                                <blockquote class="message">Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua butcher retro keffiyeh dreamcatcher synth.</blockquote>
                                                                <br />
                                                                <p class="url">
                                                                    <span class="fs1" aria-hidden="true" data-icon=""></span>
                                                                    <a href="#" data-original-title="">Read more...</a>
                                                                </p>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            
                                                            <div class="message_date">
                                                                <h3 class="date text-info">24</h3>
                                                                <p class="month">May</p>
                                                            </div>
                                                            <div class="message_wrapper">
                                                                <p>Topic title</p>
                                                                <h5 class="heading">15-06-2015 - 16:25:33  </h5>
                                                                <blockquote class="message">Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua butcher retro keffiyeh dreamcatcher synth.</blockquote>
                                                                <br />
                                                                <p class="url">
                                                                    <span class="fs1 text-info" aria-hidden="true" data-icon=""></span>
                                                                    <a href="#"></i> Read more... </a>
                                                                </p>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            
                                                            <div class="message_date">
                                                                <h3 class="date text-error">21</h3>
                                                                <p class="month">May</p>
                                                            </div>
                                                            <div class="message_wrapper">
                                                                <p>Topic title</p>
                                                                <h5 class="heading">11-06-2015 - 16:25:33  </h5>
                                                                
                                                                <blockquote class="message">Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua butcher retro keffiyeh dreamcatcher synth.</blockquote>
                                                                <br />
                                                                <p class="url">
                                                                    <span class="fs1" aria-hidden="true" data-icon=""></span>
                                                                    <a href="#" data-original-title="">Read more...</a>
                                                                </p>
                                                            </div>
                                                        </li>

                                                    </ul>
                                                    <!-- end forum activity -->

                                                </div>
                                                
                                                <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">
                                                   
                                                        <ul class="list-group">
                                                            <i class="fa fa-user"></i>
                                                            <label>Username</label>
                                                            <li class="list-group-item">{$data.user.username}</li>
                                                            <br>

                                                            <i class="fa fa-asterisk"></i>
                                                            <label>Name</label>
                                                            <li class="list-group-item">{$data.user.name}</li>
                                                            <br>
                                                            
                                                            <i class="fa fa-at"></i>
                                                            <label>Email</label>
                                                            <li class="list-group-item">{$data.user.email}</li>
                                                            <br>
                                                            
                                                            <i class="fa fa-phone"></i>
                                                            <label>Phone number</label>
                                                            <li class="list-group-item">{$data.user.phone}</li>
                                                            <br>
                                                            
                                                        </ul>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

{include file='footer.tpl'}




  
