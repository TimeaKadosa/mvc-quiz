
{include file='header.tpl'}

    <div class="container body">


        <div class="main_container">

            {include file='side_menu.tpl'}

            {include file='top_navigation.tpl'}


        

<!-- page content -->

            <div class="row">
                <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="tile-stats" style="text-decoration:none;">
                        <a href="" style="text-decoration:none;">
                        <div class="icon"><i class="fa fa-book"></i>
                        </div>
                        <h1>Course</h1>

                        <h4>Chaptors and Lessons</h4>
                        <p>Lorem ipsum psdea itgum rixt.</p>
                        </a>
                    </div>
                </div>
                <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="tile-stats">
                        <a href="" style="text-decoration:none;">
                        <div class="icon"><i class="fa fa-question"></i>
                        </div>
                        <h1>Exercises</h1>

                        <h4>Quizzes</h4>
                        <p>Lorem ipsum psdea itgum rixt.</p>
                        </a>
                    </div>
                </div>
                <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="tile-stats">
                        <a href="" style="text-decoration:none;">
                        <div class="icon"><i class="fa fa-check"></i>
                        </div>
                        <h1>Flashcards</h1>

                        <h4>Study</h4>
                        <p>Lorem ipsum psdea itgum rixt.</p>
                        </a>
                    </div>
                </div>
                <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="tile-stats">
                        <a href="" style="text-decoration:none;">
                            <div class="icon"><i class="fa fa-comments-o"></i>
                            </div>
                            <h1>Forums</h1>

                            <h4>Topics</h4>
                            <p>Lorem ipsum psdea itgum rixt.</p>
                        </a>
                    </div>
                </div>
            </div>

            <br><hr>

            <div class="row">
                        
                <div class="col-md-12 col-sm-8 col-xs-12">

                    <div class="row">

                        <div class="col-md-4 col-xs-12 widget widget_tally_box">
                            <div class="x_panel ui-ribbon-container fixed_height_390">
                                <div class="ui-ribbon-wrapper">
                                    <div class="ui-ribbon">
                                       Plan
                                    </div>
                                </div>
                                <div class="x_title">
                                    <h2>Day 18</h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">

                                    <div style="text-align: center; margin-bottom: 17px">
                                        <span class="chart" data-percent="55">
                                <span class="percent"><canvas style="height: 95px; width: 95px;" width="142" height="142"></canvas></span>
                                        </span>
                                    </div>
                                    <a href="" style="text-decoration:none;">
                                        <h3 class="name_title">Study progress</h3>
                                        <p>Open planner</p>
                                    </a>
                                    <div class="divider"></div>

                                    <p>Based on your activity to date, your readiness score is 8%. If you continue at this pace, you will need 6 months, 3 weeks, 6 days, to achieve a readiness score of 100%. Your current estimated completion date is 14-Jun-16. </p>

                                </div>
                            </div>
                        </div>

                        <div class="col-md-8 col-sm-4 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Forum Activities</h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <div class="dashboard-widget-content">

                                        <ul class="list-unstyled timeline widget">
                                            <li>
                                                <div class="block">
                                                    <div class="block_content">
                                                        <h2 class="title">
                                                            <a>Who Needs Sundance When You’ve Got&nbsp;Crowdfunding?</a>
                                                        </h2>
                                                        <div class="byline">
                                                            <span>13 hours ago</span> by <a>Jane Smith</a>
                                                        </div>
                                                        <p class="excerpt">Film festivals used to be do-or-die moments for movie makers. They were where you met the producers that could fund your project, and if the buyers liked your flick, they’d pay to Fast-forward and… <a>Read&nbsp;More</a>
                                                        </p>
                                                    </div>
                                                </div>
                                            </li>

                                            <li>
                                                <div class="block">
                                                    <div class="block_content">
                                                        <h2 class="title">
                                                            <a>Who Needs Sundance When You’ve Got&nbsp;Crowdfunding?</a>
                                                        </h2>
                                                        <div class="byline">
                                                            <span>13 hours ago</span> by <a>Jane Smith</a>
                                                        </div>
                                                        <p class="excerpt">Film festivals used to be do-or-die moments for movie makers. They were where you met the producers that could fund your project, and if the buyers liked your flick, they’d pay to Fast-forward and… <a>Read&nbsp;More</a>
                                                        </p>
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="block">
                                                    <div class="block_content">
                                                        <h2 class="title">
                                                            <a>Who Needs Sundance When You’ve Got&nbsp;Crowdfunding?</a>
                                                        </h2>
                                                        <div class="byline">
                                                            <span>13 hours ago</span> by <a>Jane Smith</a>
                                                        </div>
                                                        <p class="excerpt">Film festivals used to be do-or-die moments for movie makers. They were where you met the producers that could fund your project, and if the buyers liked your flick, they’d pay to Fast-forward and… <a>Read&nbsp;More</a>
                                                        </p>
                                                    </div>
                                                </div>
                                            </li>
                                                <li>
                                                    <div class="block">
                                                        <div class="block_content">
                                                            <h2 class="title">
                                                                <a>Who Needs Sundance When You’ve Got&nbsp;Crowdfunding?</a>
                                                            </h2>
                                                            <div class="byline">
                                                                <span>13 hours ago</span> by <a>Jane Smith</a>
                                                            </div>
                                                            <p class="excerpt">Film festivals used to be do-or-die moments for movie makers. They were where you met the producers that could fund your project, and if the buyers liked your flick, they’d pay to Fast-forward and… <a>Read&nbsp;More</a>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        
<!-- chart js -->
    <script src="/assets/themeGentelella/js/chartjs/chart.min.js"></script>
    
    <!-- easypie -->
    <script src="/assets/themeGentelella/js/easypie/jquery.easypiechart.min.js"></script>
    <script>
        $(function () {
            $('.chart').easyPieChart({
                easing: 'easeOutElastic',
                delay: 3000,
                barColor: '#26B99A',
                trackColor: '#fff',
                scaleColor: false,
                lineWidth: 20,
                trackWidth: 16,
                lineCap: 'butt',
                onStep: function (from, to, percent) {
                    $(this.el).find('.percent').text(Math.round(percent));
                }
            });
            var chart = window.chart = $('.chart').data('easyPieChart');
            $('.js_update').on('click', function () {
                chart.update(Math.random() * 200 - 100);
            });
        });
    </script>

  
{include file='footer.tpl'}

