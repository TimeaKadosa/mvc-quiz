	<form style="display:none;"  id="image-upload" action="/upload" method="post" enctype="multipart/form-data">

        <h3>Change the avatar</h3>
	    
        <div class="profileImg">
	    	<div class="avatar-view" title="Change the avatar">

				<img src="/profileImages/{if $data.user.image == ''}user-default.jpg{else}{$data.user.image}{/if}" alt="Avatar">
        	</div>
	    </div>

        <div id="fileName"></div>

        <div class="profileImgButtons">
            <!-- choose a file-->
            <span id="upload" class="btn btn-default btn-file" onclick="changeButton();">
                    Browse <input type="file"  name="fileToUpload">
            </span>
            <!-- submit -->
            <input id="submit" type="submit" value=" Upload " class="btn btn-success" name="submit" style="display:none;">

            <!-- cancel -->
            <button id="cancel" type="button" class="btn btn-danger" onclick="closeDialog();">Cancel</button>
        
        </div>

    </form>


        <!-- Avatar popup -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

<script src="/assets/custom/js/jquery.lightbox_me.js"></script>
<script src="/assets/custom/js/custom_lightbox_me.js"></script>
