

{include file='header.tpl'}

<body style="background:#F7F7F7;">
    
    <div class="">
        <a class="hiddenanchor" id="toregister"></a>
        <a class="hiddenanchor" id="tologin"></a>

        <div id="wrapper">
            <div id="login" class="animate form">
                <section class="login_content">

                    {if isset($smarty.session.success.message) }

                                    <div class="alert alert-success">
                                        <div class="info">{$smarty.session.success.message}</div>
                                    </div>
                            {elseif isset($message['error']) }
                                    <div class="alert alert-danger">
                                        <div class="info">{$message['error']}</div>
                                    </div>
                            {/if}

            

                    <!-- cleare message session -->
                   
                    <form action="" method="post">
                        <h1>Login</h1>
                        <div>
                            <input type="text" class="form-control" placeholder="Username" required="" name="username" />
                        </div>
                        <div>
                            <input type="password" class="form-control" placeholder="Password" required="" name="password" />
                        </div>
                        <div>
                            <input type="submit" value="Log in" class="btn btn-default submit" >
                            <a class="reset_pass" href="#">Lost your password?</a>
                        </div>
                        <div class="clearfix"></div>
                        <div class="separator">

                            <p class="change_link">New to site?
                                <a href="#toregister" class="to_register"> Create Account </a>
                            </p>
                            <div class="clearfix"></div>
                            <br />
                            <div>
                                <h1><i class="fa fa-graduation-cap" style="font-size: 26px;"></i> Codeifyr</h1>

                                <p>©2015 All Rights Reserved. </p>
                            </div>
                        </div>
                    </form>
                    <!-- form -->
                </section>
                <!-- content -->
            </div>
            <div id="register" class="animate form">
                <section class="login_content">
                    <form action="/users" method="post">
                        <h1>Create Account</h1>
                        <div>
                            <input type="text" name="name" class="form-control" placeholder="Name" required="" />
                        </div>
                        <div>
                            <input type="text" name="username" class="form-control" placeholder="Username" required="" />
                        </div>
                        <div>
                            <input type="email" name="email" class="form-control" placeholder="Email" required="" />
                        </div>
                        <div>
                            <input type="text" name="phone" class="form-control" placeholder="Phone number" required="" />
                        </div>

                        <div>
                            <input type="password" name="password" class="form-control" placeholder="Password" required="" />
                        </div>
                        <div>
                            <input type="hidden" name="type" value="single" />
                            <input type="submit" value="Submit" class="btn btn-default submit" >
                        </div>
                        <div class="clearfix"></div>
                        <div class="separator">

                            <p class="change_link">Already a member ?
                                <a href="#tologin" class="to_register"> Log in </a>
                            </p>
                            <div class="clearfix"></div>
                            <br />
                            <div>
                                <h1><i class="fa fa-graduation-cap" style="font-size: 26px;"></i> Codeifyr</h1>

                                <p>©2015 All Rights Reserved. </p>
                            </div>
                        </div>
                    </form>
                    <!-- form -->
                </section>
                <!-- content -->
            </div>
        </div>
    </div>

