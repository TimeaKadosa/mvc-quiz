{*$user|@var_dump*}
{*$user.username*}
{include file='header.tpl'}


	<form style="display:none;"  id="image-upload" action="/upload" method="post" enctype="multipart/form-data">

        <h3>Change the avatar</h3>
	    
        <div class="profileImg">
	    	<div id="test" class="avatar-view" title="Change the avatar">

				<img src="/profileImages/user-default.jpg" alt="Avatar">
        	</div>
	    </div>

        <div id="fileName"></div>

        <div class="profileImgButtons">
            <!-- choose a file-->
            <span id="upload" class="btn btn-default btn-file" onclick="changeButton();">
                    Browse <input type="file"  name="fileToUpload">
            </span>
            <!-- submit -->
            <input id="submit" type="submit" value=" Upload " class="btn btn-success" name="submit" style="display:none;">

            <!-- cancel -->
            <button id="cancel" type="button" class="btn btn-danger" onclick="closeDialog();">Cancel</button>
        
        </div>

    </form>

<p id="popup">Click me</p>

 </div>


{include file='footer.tpl'}



