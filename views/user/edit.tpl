
{include file='header.tpl'}


    <div class="container body">


        <div class="main_container">

            {include file='side_menu.tpl'}
            {include file='top_navigation.tpl'}



            <div class="page-title">
                <div class="title_left">
                    <h3>Edit Profile</h3>
                </div>
    		
            </div>  

			<div class="row">

    			<div class="col-md-12 col-sm-12 col-xs-12">

    				<!-- box style -->
        			<div class="x_panel">
            
			            <div class="x_title">
			                <h2>User Profile</h2>

			            	<div class="clearfix"></div>
			            </div>
			
						<div class="col-md-4 col-sm-4 col-xs-12 profile_left">
							
							<!-- profile image -->
		 					<div class="profile_img">
    
							    <div id="crop-avatar">
							        <!-- Current avatar -->
							        <div id="popup" class="avatar-view" title="Change the avatar">
							            <img src="/profileImages/{if $data.user.image != ''}{$data.user.image}{else}user-default.jpg{/if}" alt="Avatar">

							        </div>

							        {include file='user/partials/profile_img_change.tpl'}
							    </div>

							</div>
							<!-- /profile image -->
		 					<br>

		 					<ul class="list-unstyled user_data">
                                <li><i class="fa fa-map-marker user-profile-icon"></i> San Francisco, California, USA
                                </li>

                                <li>
                                    <i class="fa fa-at"></i> {$data.user.email}
                                </li>

                                <li class="m-top-xs">
                                    <i class="fa fa-phone"></i>
                                    {$data.user.phone}
                                </li>
                            </ul>

                            {if isset($message['success']) }

                            		<div class="alert alert-success">
			                        	<div class="info">{$message['success']}</div>
			                    	</div>
                            {elseif isset($message['error']) }
  									<div class="alert alert-success">
			                        	<div class="info">{$message['error']}</div>
			                    	</div>
							{/if}

                            

<!-- 			            				unset($_SESSION['error']['message']); -->
			        	

		 					
            			</div>



            			<div class="col-md-8 col-sm-8 col-xs-12">
			            <form action="/users/{$data.user.id}" method="post">

			            	<div class="form-group">
			            		 <i class="fa fa-user"></i>
							    <label for="username">Username</label>
							    <input type="text" class="form-control" name="username" id="" value="{$data.user.username}">
						  	</div>	
							<div class="form-group">
								<i class="fa fa-asterisk"></i>
								<label for="name">Name</label>
								<input type="text" name="name" class="form-control" id="" value="{$data.user.name}">
							</div>

							<div class="form-group">
								<i class="fa fa-at"></i>
								<label for="email">Email</label>
								<input type="email" name="email" class="form-control" id="" value="{$data.user.email}">
							</div>

							<div class="form-group">
								<i class="fa fa-phone"></i>
							    <label for="phone">Phone number</label>
							    <input type="text" name="phone" class="form-control" id="" value="{$data.user.phone}">
							</div>

							<div class="form-group">
								<i class="fa fa-lock"></i>
							    <label for="password">Password</label>
							    <input type="password" name="password" class="form-control" id="" value="{$data.user.password}">
							</div>

			 
							 <input type="submit" value="Submit" class="btn btn-info submit" >
						</form>
					</div>

					</div>
					<!-- /box style -->
				</div>
			</div>

		</div>
	</div>


{include file='footer.tpl'}