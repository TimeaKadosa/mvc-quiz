<!-- Side menu -->
            <div class="col-md-3 left_col">

                <div class="left_col scroll-view">

                    <div class="navbar nav_title" style="border: 0;">
                        <a href="index.html" class="site_title"><i class="fa fa-graduation-cap"></i> <span>Codeifyr</span></a>
                    </div>
                    <div class="clearfix"></div>

                    <!-- menu prile quick info -->
                    <div class="profile">
                        <div class="profile_pic">
                            <img src="/profileImages/{if $data.user.image != ''}{$data.user.image}{else}user-default.jpg{/if}" alt="" class="img-circle profile_img">
                        </div>
                        <div class="profile_info">
                            <span>Welcome,</span>
                            <h2>{$data.user.username}</h2>
                        </div>
                    </div>
                    <!-- /menu prile quick info -->

                    <br />

                    <!-- /sidebar menu  -->
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                        <div class="menu_section">
                         <br><hr>
                            <ul class="nav side-menu">
                                
                                <li><a href="/user"><i class="fa fa-home"></i> Home </a></li>
                                   
                                <li><a><i class="fa fa-book"></i> Course <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="form.html">Pre-Assesment</a>
                                        </li>
                                        <li><a href="form_advanced.html">Chapters & Lessons </a>
                                        </li>
                                        <li><a href="form_validation.html">Post-Assesment</a>
                                        </li>
                                    </ul>
                                </li>

                                <li><a href="e_commerce.html"><i class="fa fa-edit"></i> Exercises & quizzes </a></li>
                                <li><a href="e_commerce.html"><i class="fa fa-list-alt"></i> Flashcards </a></li>
                                <li><a href="e_commerce.html"><i class="fa fa-comments-o"></i> Forums </a></li>
                                
                                
                            </ul>
                        </div>
                        <div class="menu_section">
                            <h3>Personal</h3>
                            <ul class="nav side-menu">
                                <li><a><i class="fa fa-user"></i> Account <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="/user/{$data.user.id}">My Profile</a>
                                        </li>
                                        <li><a href="project_detail.html">Study Planner</a>
                                        </li>
                                       
                                    </ul>
                                </li>
                               
                            </ul>
                        </div>


                        <div class="menu_section">
                            <h3>Codeifyr</h3>
                            <ul class="nav side-menu">
                                <li><a href="e_commerce.html"><i class="fa fa-university"></i> About us </a></li>
                                <li><a href="e_commerce.html"><i class="fa fa-star"></i> Features </a></li>
                                <li><a href="e_commerce.html"><i class="fa fa-envelope"></i> Contact </a></li>
                       
                                
                            </ul>
                        </div>

                    </div>
                    <!-- /sidebar menu -->

                    
                </div>
            </div>

            