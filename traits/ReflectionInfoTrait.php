<?php

namespace Codeifyr\Traits;

	/**
	 * Use reflection to get information about a class or an object
	 */

	trait ReflectionInfoTrait {

		/**
		 * Returns the class base name, "user" for UserController
		 * @return String	The class base name
		 */

		public function reflectBaseName() {

			# Get the class's shortname

			$Reflection = new \ReflectionClass($this);
			$ClassName 	= $Reflection->getShortName();


			# Split the class name on Capitals
			# Splits camel cased parts
			# Returns an array with the parts of the className
			$classNameParts = explode(' ', preg_replace( '/([a-z0-9])([A-Z])/', "$1 $2", $ClassName));


			if (count($classNameParts) > 2) {

				array_pop($classNameParts);
				$classNameParts = implode('', $classNameParts);

				return $baseName = lcfirst($classNameParts);
			}
			# Lower case the first part of the class name
			$baseName = strtolower($classNameParts[0]);

			# Return
			return $baseName;
		}

	}