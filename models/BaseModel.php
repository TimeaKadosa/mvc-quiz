<?php

namespace Codeifyr\Models;

use Codeifyr\Services\Database;
use Codeifyr\Services\Password;

	class BaseModel {

		private $Database;

		protected $table;

		public function __construct() {


		}

		public function fetchAll () {

			# Prepare and execute query
			$sql = "SELECT * FROM " . $this->table;
			$stmt = Database::conn()->prepare($sql);
			$stmt->execute();
			
			# Fetch all records into an array
			$collection = $stmt->fetchAll();

			# Return the collection
			return $collection;
		}

		public function fetch($id) {

			$sql = "SELECT * FROM user WHERE id = :id";
			$stmt = Database::conn()->prepare($sql);
			
			# Execute query
			$stmt->execute(array('id' => $id));
			
			# Fetch a row
			$row = $stmt->fetch();

			# Return the row
			return $row;


		}

		public function insert($input) {

			
			
			# Build insert columns and prepared values
			$columns 	= implode(', ', array_keys($input));
			
			$prepared 	= ':' . implode(', :', array_keys($input));

			$input['password'] = Password::password_encrypt($input['password']);
			//$input['password'] = password_hash($input['password'], PASSWORD_DEFAULT);

			# Prepare query
			echo $sql = "INSERT INTO " . $this->table . " (" . $columns . ") VALUES (" . $prepared . ")";
			$db = Database::conn()->prepare($sql);

			# Execute query
			if($db->execute($input)) {

				return true;

			} else {

				return false;
			}
		}



		public function update ($input, $id) {

			if (isset($input['password'])) {
				
				$input['password'] = Password::password_encrypt($input['password']);
			}
			

			$update = implode(",", array_map(function($a, $b) { return sprintf(" $b = '$a' ");}, $input , array_keys($input)));
			# Prepare query

			$sql = "UPDATE " . $this->table . " SET " . $update . " WHERE id = " . $id;

			$stmt = Database::conn()->prepare($sql);

			# Execute query
			if($stmt->execute($input)) {
				return true;
			} else {
				return false;
			}
		}

		public function getLastInsertId() {

			return $id = Database::conn()->lastInsertId();
		}

		public function execute($sql, $binding) {

			$db = Database::conn()->prepare($sql);
			$db->execute($binding);

			return true;
		}





	}