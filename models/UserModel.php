<?php

namespace Codeifyr\Models;

use Codeifyr\Models\BaseModel;
use Codeifyr\Services\Database;
use Codeifyr\Services\Password;

	class UserModel extends BaseModel {

		# 1 set the table name
		protected $table = "user";

		# 2 set the validation rules
		protected $validationRules = [
			'id'		=> 'numeric',
			'name'		=> 'required',
			'email'		=> 'required|valid_email',
			'username'	=> 'required|min_len,4|max_len,16',
			'password'	=> 'min_len,4|max_len,16'
		];


		public function getUserByUsernamePassword($username, $password) {

			$sql = "SELECT * FROM user WHERE username = :username AND password = :password" ;
			$binding = array('username' => $username, 'password' => $password );
			# get Db connection and prepare the sql
			$db = Database::conn()->prepare($sql);
			# execute query
			$db->execute($binding);
			# fetch a row
			$row = $db->fetch();

			# return the row

			return $row; 
		}

		



	}