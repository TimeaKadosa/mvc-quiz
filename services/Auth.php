<?php 

namespace Codeifyr\Services;

	class Auth {

		/*
		 * This class can manage accounts and authenticate users.
		 * It can perform several types of operations to manage the records of users stored 
		 * in a MySQL database (accessed using the MySQLi extension) like creating user accounts,
		 * activate accounts, change the user e-mail address or the password, and remove accounts.
		 * 
		 * The class can also authenticate an user with a given password, create user sessions,
		 * delete sessions, get the session information, check if the session is valid, manage failed 
		 * login attempts.
		 */

		public function __construct() {}
	}