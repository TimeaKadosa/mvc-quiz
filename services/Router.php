<?php

namespace Codeifyr\Services;


	class Router {


		protected static $Rout;

		public function __construct() {}


		public static function setRout($rout, $controller, $req){
			
			$uri 		= $_SERVER['REQUEST_URI'];
			$request 	= $_SERVER['REQUEST_METHOD'];

			// get predefined Controller and its method
			$controllerParts = explode("@", $controller);
			// get predefined rout parts
			$routParts = explode("/", $rout);
			// get uri parts
			$uriParts = explode("/", $uri);

			$id = null;

			// check if the 2nd part of the uri is set and if it's numeric
			// that means we have an id
			if (isset($uriParts[2]) && is_numeric($uriParts[2])) {

				// set the id from the uri
				$id = $uriParts[2];
				// invert 
				$uriParts[2] = ":id";
				$uri = implode("/", $uriParts);


			}
			
			// set variables for the controller and its method
			$controller 	= $controllerParts[0];
			$method 		= $controllerParts[1];

			// check if the uri matches with the predefined rout
			// check if the requested method matches with the predefined request
			if ($uri == $rout && $request == $req) {

				// set the dynamic name for the controller
				$Controller = "\Codeifyr\Controllers\\" . $controller;


				// instantiate the current controller
				$controllerClass = new  $Controller();
				// call the expected method
				self::$Rout = $controllerClass->$method($id);

			}


		}

		public static function getRout() {

			return self::$Rout;
		}

	}