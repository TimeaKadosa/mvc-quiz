<?php

namespace Codeifyr\Services;

use Exception;



	class View {

		private static $viewFile;
		private static $viewData;
		private static $viewMessage;
		
		public static function setView($view, $data = null, $message = null) {

			try {

				if (!is_string($view)) {
					throw new Exception ('View says: view should be a string. ');
				}

				if (!is_null($data) && !is_array($data)) {
					throw new Exception ('View says: data should be an array.');
				}

				self::$viewFile 	= $view;
				self::$viewData 	= $data;
				self::$viewMessage 	= $message;

				return true;
			}
			catch(Exception $e) {

				echo $e->getMessage();
			}

		}


		public static function displayView($smarty) {


			$smarty->setTemplateDir(DOC_ROOT.'/views');
			$smarty->setCompileDir('/Smarty/templates_c');
			$smarty->setCacheDir('/Smarty/cache');
			$smarty->setConfigDir('/Smarty/configs');


			try{

				if (!file_exists(DOC_ROOT. '/views/' .self::$viewFile) || is_dir(DOC_ROOT. '/' .self::$viewFile)) {
					throw new Exception('View says: view should be a file.');
					
				}

				if (!is_null(self::$viewData) && !is_array(self::$viewData)) {
					throw new Exception('View says: Display data should be an array.');
					
				}

				$smarty->assign('data' , self::$viewData);
				$smarty->assign('message' , self::$viewMessage);
				$smarty->display(self::$viewFile);

				return true;

			}
			catch(Exception $e) {

				echo $e->getMessage();
			}
			

				
		}


	}