
    $('#popup').click(function(e) {
        $('#image-upload').lightbox_me({
            centered: true, 
            onLoad: function() { 
                $('#sign_up').find('input:first').focus()
                }
            });

        $("#fileName").html("<p></p>");
        $('#upload').show();
        $('#submit').hide();

        e.preventDefault();
    });


    function closeDialog(){
        
        $('#image-upload').trigger('close');
    }

    function changeButton(){

        $('#upload').hide();
        $('#submit').show();
    }


    $(document).on('change', '.btn-file :file', function() {
        var input = $(this),
            numFiles = input.get(0).files ? input.get(0).files.length : 1,
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [numFiles, label]);
    });


    $(document).ready( function() {
        $('.btn-file :file').on('fileselect', function(event, numFiles, label) {
            console.log(numFiles);
            console.log(label);

            $("#fileName").html("<p>" + label + "</p>");
        });
    });