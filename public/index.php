<?php
	/*
	 * System config and initialization
	 */
	
	include ('../init.inc.php');

	use Codeifyr\Services\Router;
	use Codeifyr\Services\View;

	$smarty = new Smarty();

	/*
	 * Home
	 */

	Router::setRout('/', 'HomeController@index','GET'); 
	Router::setRout('/home', 'HomeController@index','GET'); 

	/*
	 * Test
	 */

	Router::setRout('/test', 'UserController@test','GET'); 
	Router::setRout('/upload', 'UserController@upload','POST'); 

	/*
	 * User
	 */

	Router::setRout('/user', 'UserController@index','GET'); 				// GET
	Router::setRout('/user/create', 'UserController@create','GET'); 		// GET
	Router::setRout('/user/:id', 'UserController@show','GET'); 				// GET
	Router::setRout('/user/:id/edit', 'UserController@edit','GET'); 		// GET
	Router::setRout('/user/:id/delete', 'UserController@delete','GET'); 	// GET
	Router::setRout('/user/:id/upload', 'UserController@upload','POST'); 	// GET

	Router::setRout('/user', 'UserController@store','POST'); 				// POST
	Router::setRout('/user/:id', 'UserController@update','POST'); 			// POST



	/*
	 * Login / Logout
	 */

	Router::setRout('/login', 'UserController@loginform', 'GET');
	Router::setRout('/login', 'UserController@login', 'POST');
	Router::setRout('/logout', 'UserController@logout', 'GET');

	/*
	 * Signup
	 */

	


	
	// check if the rout exists
	// if not display error

	// $router->setBasePath(WEB_ROOT);
	// $route = $router->matchCurrentRequest();
	$rout = Router::getRout();

	/*if ($rout != true) {
		View::setView('views/error.php', array('message' => 'No such route exists.','test' => 'Test message'));
	}*/

	/*
	 * Display the view
	 */
	View::displayView($smarty);


